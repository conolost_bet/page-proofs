(function($) {
  $(function() {
    $(".menu_icon").on("click", function() {
      $(this)
        .closest(".menu")
        .toggleClass("menu_state_open");
    });

    $("li").on("click", function() {
      // do something

      $(this)
        .closest(".menu")
        .removeClass("menu_state_open");
    });
  });
})(jQuery);

$('.logo').click(function() {
    location.reload();
});

$(document).ready(() => {
  $("#scroll").click(() => {
    f_scroll();
  });
});
function f_scroll() {
  $("html,body").animate({
    scrollTop: $(window).scrollTop() + 550
  });
}
